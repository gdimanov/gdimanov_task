### How do I get set up? ###

* set FLASK_APP=run.py
* pip install -r requirements.txt
* flask db upgrade
* Start local redis server
* celery worker -A app.celery_tasks.celery --loglevel=info
* set FLASK_APP=run.py (in new console as celery will continue running in the previous)
* flask run

### Info ###

* if you check test run time is now ~4s
* in the previous commit its below 0.1s

### What would I improve ###

* swagger to document the api endpoints
* extended field validation and do it through flask-wtf
* make the api more informative -> currently on post if contact already exists we just recieve code 403
* send celery tasks only when using dev/prod configuration -> this is whats causing tests to run slow

### Api "documentation" ###
PUT and POST endpoints expect json.

/contacts

    [GET] - returns list of all contacts
    [POST] - create user. expected {"username": "X", "first_name": "Y", "last_name": "Z", "emails":["em1", "em2"]}
            emails is optional
/contacts/<str:username>

    [GET] - return info for the contact username
    [PUT] - update the entity. You should provide the whole record like in /contacts POST
    [DELETE] - deletes the entity
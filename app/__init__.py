from config import app_config, CELERY_BROKER_URL
from flask import Flask
from celery import Celery
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy


# initialize sql-alchemy
db = SQLAlchemy()
migrate = Migrate()
celery = Celery(__name__, broker=CELERY_BROKER_URL)


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app_conf = app_config[config_name]
    app.config.from_object(app_conf)
    from app import models
    with app.app_context():
        db.init_app(app)
        celery.conf.update(app.config)
    migrate.init_app(app, db)
    from app.routes import contact_bp
    app.register_blueprint(contact_bp)

    from .celery_tasks import create_contact
    create_contact.delay()

    return app

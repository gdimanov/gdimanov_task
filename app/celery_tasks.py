from . import celery
from . import models
import time


@celery.task()
def create_contact():
    while True:
        time.sleep(15)
        contact = models.Contact.create_random_contact()
        contact.save()


@celery.task
def delete_old_contact(contact_id):
    models.Contact.get(contact_id).delete()

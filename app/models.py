import random
import string
from app import db
from datetime import datetime, timezone


class Contact(db.Model):
    __tablename__ = 'contact'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.now(timezone.utc))
    emails = db.relationship('Email', backref='contact', lazy='joined')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_by_username(username):
        return Contact.query.filter(Contact.username == username).first()

    @staticmethod
    def get_all():
        return Contact.query.all()

    @staticmethod
    def exists(username):
        return bool(Contact.query.filter(Contact.username == username).first())

    @staticmethod
    def get_random_contact():
        def get_r_str():
            return "".join(random.choice(string.ascii_uppercase) for i in range(10))
        return Contact(username=get_r_str(), first_name=get_r_str(), last_name=get_r_str())

    def to_dict(self):
        return {"username": self.username, "first_name": self.first_name, "last_name": self.last_name, "emails": [email.email for email in self.emails]}

    def __repr__(self):
        return f"<Contact: {self.username}>"


class Email(db.Model):
    __tablename__ = 'email'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), nullable=False)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

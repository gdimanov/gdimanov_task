from flask import Blueprint, request, abort, jsonify
from .celery_tasks import delete_old_contact
from app import models

contact_bp = Blueprint('contacts', __name__, url_prefix="/contacts")


def check_rq_json(rq_json):
    if not rq_json or type(rq_json) != dict:
        return False
    uname = rq_json.get("username")
    fname = rq_json.get("first_name")
    lname = rq_json.get("last_name")
    return all([uname, fname, lname])


@contact_bp.route('/', methods=['POST', 'GET'])
def contacts():
    if request.method == "POST":
        rq = request.json
        if not check_rq_json(rq):
            abort(400)

        username = rq.get("username")
        # check if such contact already exists
        if models.Contact.exists(username):
            abort(403)  # Forbidden
        first_name = rq.get("first_name")
        last_name = rq.get("last_name")
        contact = models.Contact(username=username, first_name=first_name, last_name=last_name)
        if "emails" in rq and type(rq["emails"]) == list:
            contact.emails = [models.Email(email=em, contact=contact) for em in rq["emails"]]
        contact.save()
        # send a task to delete after a minute
        delete_old_contact.apply_async((contact.id,), countdown=60)
        response = jsonify(contact.to_dict())
        response.status_code = 201
        return response

    if request.method == "GET":
        all_contacts = models.Contact.get_all()
        results = [contact.to_dict() for contact in all_contacts]
        response = jsonify(results)
        response.status_code = 200
        return response


@contact_bp.route('/<string:username>', methods=['GET', 'PUT', 'DELETE'])
def specific_contact(username):
    contact = models.Contact.get_by_username(username)
    if not contact:
        abort(404)

    if request.method == "GET":
        response = jsonify(contact.to_dict())
        response.status_code = 200
        return response

    if request.method == "PUT":
        rq = request.json
        if not check_rq_json(rq):
            abort(404)
        contact.username = rq["username"]
        contact.first_name = rq["first_name"]
        contact.last_name = rq["last_name"]
        emails = rq.get("emails", [])
        if type(emails) == list:
            contact.emails = [models.Email(email=em) for em in emails]
        contact.save()
        response = jsonify(contact.to_dict())
        response.status_code = 200
        return response

    if request.method == "DELETE":
        contact.delete()
        return "Deleted", 204

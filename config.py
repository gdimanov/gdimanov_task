import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
CELERY_BROKER_URL = 'redis://localhost:6379/0'


class Config(object):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'task.sqlite3')


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'task.sqlite3')


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'  # in-memory


app_config = {
    "dev" : DevelopmentConfig,
    "prod": ProductionConfig,
    "test": TestConfig
}

import unittest
from app import create_app, db


class TestContactCRUD(unittest.TestCase):

    def setUp(self):
        self.app = create_app(config_name="test")
        self.client = self.app.test_client

        with self.app.app_context():
            # create all tables
            db.create_all()

    def tearDown(self):
        """teardown all initialized variables."""
        with self.app.app_context():
            # drop all tables
            db.session.remove()
            db.drop_all()

    """### Create contact POST tests ###"""
    def test_contact_post_correct_input_no_email(self):
        contact = {
            "username": "usr123",
            "first_name": "Name1",
            "last_name": "Name2"
        }
        res = self.client().post('/contacts/', json=contact)
        self.assertEqual(res.status_code, 201)
        contact["emails"] = []
        self.assertDictEqual(contact, res.get_json())

    def test_contact_post_already_existing(self):
        contact = {
            "username": "usr123",
            "first_name": "Name1",
            "last_name": "Name2",
        }
        res = self.client().post('/contacts/', json=contact)
        self.assertEqual(res.status_code, 201)
        res = self.client().post('/contacts/', json=contact)
        self.assertEqual(res.status_code, 403)

    def test_contact_post_wrong_input(self):
        contact = [
            ("username", "usr123"),
            ("first_name", "Name1"),
            ("last_name", "Name2")
        ]
        res = self.client().post('/contacts/', json=contact)
        self.assertEqual(res.status_code, 400)

    def test_contact_post_partial_input(self):
        # missing last_name
        contact = {
            "username": "usr123",
            "first_name": "Name1"
        }
        res = self.client().post('/contacts/', json=contact)
        self.assertEqual(res.status_code, 400)

    """### Get all contacts tests ###"""
    def test_contact_get_existing_contacts(self):
        contact1 = {
            "username": "usr1",
            "first_name": "Name1",
            "last_name": "Name2",
            "emails": []
        }
        contact2 = {
            "username": "usr2",
            "first_name": "Name11",
            "last_name": "Name22",
            "emails": []
        }
        self.client().post('/contacts/', json=contact1)
        self.client().post('/contacts/', json=contact2)

        res = self.client().get('/contacts/')
        self.assertEqual(res.status_code, 200)
        res = res.get_json()
        self.assertIn(contact1, res)
        self.assertIn(contact2, res)

    def test_contact_get_empty_contacts(self):
        res = self.client().get('/contacts/')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.get_json(), [])

    """### Get specific contact test ###"""
    def test_contact_get_existing(self):
        contact = {
            "username": "usr1",
            "first_name": "Name1",
            "last_name": "Name2",
            "emails": ["mail1@sth.st"]
        }
        self.client().post('/contacts/', json=contact)
        res = self.client().get('/contacts/usr1')
        self.assertEqual(res.status_code, 200)
        self.assertDictEqual(contact, res.get_json())

    def test_contact_get_not_found(self):
        res = self.client().get('/contacts/usr1')
        self.assertEqual(res.status_code, 404)

    """### Put specific contact test ###"""
    def test_contact_update_existing(self):
        contact = {
            "username": "usr1",
            "first_name": "Name1",
            "last_name": "Name2",
        }
        self.client().post('/contacts/', json=contact)
        contact["first_name"] = "newName"
        contact["emails"] = ["email1@asd.df"]
        res = self.client().put('/contacts/usr1', json=contact)
        self.assertEqual(res.status_code, 200)
        self.assertDictEqual(contact, res.get_json())

    def test_contact_update_existing_email_removal(self):
        contact = {
            "username": "usr1",
            "first_name": "Name1",
            "last_name": "Name2",
            "emails": ["email1@abv.bg"]
        }
        self.client().post('/contacts/', json=contact)
        contact["emails"] = []
        res = self.client().put('/contacts/usr1', json=contact)
        self.assertEqual(res.status_code, 200)
        self.assertDictEqual(contact, res.get_json())

    def test_contact_update_not_found(self):
        contact = {
            "username": "usr1",
            "first_name": "Name1",
            "last_name": "Name2",
        }
        res = self.client().put('/contacts/usr1', json=contact)
        self.assertEqual(res.status_code, 404)

    """### Delete specific contact ###"""
    def test_contact_delete_existing(self):
        contact = {
            "username": "usr1",
            "first_name": "Name1",
            "last_name": "Name2",
        }
        self.client().post('/contacts/', json=contact)
        res = self.client().delete('/contacts/usr1')
        self.assertEqual(res.status_code, 204)

    def test_contact_delete_not_found(self):
        res = self.client().delete('/contacts/usr1')
        self.assertEqual(res.status_code, 404)


if __name__ == "__main__":
    unittest.main()